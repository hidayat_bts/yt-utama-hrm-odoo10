# -*- coding: utf-8 -*-
# @Author: xrix
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from openerp.exceptions import except_orm

from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
import math

import time
import calendar
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, exceptions, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_is_zero, float_compare
from odoo.exceptions import ValidationError, RedirectWarning, UserError
from collections import OrderedDict

import xlsxwriter
import base64
from cStringIO import StringIO
import pytz
from pytz import timezone
import PIL
import io


class purchaseConfirmWizard(models.TransientModel):
    _name = 'slip.payroll'

    warning = fields.Char(readonly=True)
    file_data = fields.Binary('File', readonly=True)

    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone('Asia/Jakarta'))

    @api.multi
    def download_slip(self):
        fp = StringIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet('Report Payroll')
        worksheet.set_column('A:A', 12)
        worksheet.set_column('B:D', 3)
        worksheet.set_column('E:E', 12)
        worksheet.set_column('F:F', 3)
        worksheet.set_column('G:G', 17)
        worksheet.set_column('H:H', 3)

        worksheet.set_column('I:I', 12)
        worksheet.set_column('J:K', 3)
        worksheet.set_column('M:M', 12)
        worksheet.set_column('N:N', 3)
        worksheet.set_column('O:O', 17)
        worksheet.set_column('P:P', 3)

        worksheet.set_column('Q:Q', 12)
        worksheet.set_column('R:T', 3)
        worksheet.set_column('U:U', 12)
        worksheet.set_column('V:V', 3)
        worksheet.set_column('W:W', 17)
        worksheet.set_column('X:X', 3)

        worksheet.set_column('Y:Y', 12)
        worksheet.set_column('Z:AB', 3)
        worksheet.set_column('AC:AC', 12)
        worksheet.set_column('AD:AD', 3)
        worksheet.set_column('AE:AE', 17)
        worksheet.set_column('AF:AF', 3)

        #konten di sini
        active_ids = self._context.get('active_ids')
        desain_ids = self.env['hr.payslip'].browse(active_ids)
        brand_ids = desain_ids.mapped('name')

        merge_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'white'
        })
        merge_format.set_font_color('red')
        title_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'valign': 'vcenter',
        })
        left_format = workbook.add_format({
            'bold': 0,
            'border' : 1,
            'valign' : 'vleft',
        })
        left2_format = workbook.add_format({
            'bold': 0,
            'border' : 0,
            'valign' : 'vleft',
        })
        center_format = workbook.add_format({
            'bold': 0,
            'border' : 0,
            'align' : 'center',
            'valign' : 'vcenter',
        })
        center1_format = workbook.add_format({
            'bold': 1,
            'border' : 0,
            'align' : 'center',
            'valign' : 'vcenter',
        })
        title_format.set_text_wrap()
        right_format = workbook.add_format({
            'bold' : 0,
            'border': 0,
            'align' : 'right',
            'valign': 'vright',
        })
        right1_format = workbook.add_format({
            'bold' : 1,
            'border': 0,
            'align' : 'right',
            'valign': 'vright',
        })

        row = 1
        #for brand_id in brand_ids :
        merge_from = row
        merge_to = row+1
        row += 2
        worksheet.merge_range('A%s:AB%s'%(merge_from,merge_to), 'Payroll', title_format)

        #desain_brand_ids = desain_ids.filtered(lambda desain: desain.brand_id.id == brand_id.id)
        desain_brand_ids = desain_ids
        while desain_brand_ids :
            five_desain_ids = desain_brand_ids[0:4]
            col = {1:['A','B','C','D','E','F','G','H'], 2:['I','J','K','L','M','N','O','P'], 3:['Q','R','S','T','U','V','W','X'], 4:['Y','Z','AA','AB','AC','AD','AE','AF']}
            col_range = 1
            row += 1
            for desain in five_desain_ids :
                one_letter = col[col_range][0]
                two_letter = col[col_range][1]
                tree_letter = col[col_range][2]
                four_letter = col[col_range][3]
                five_letter = col[col_range][4]
                six_letter = col[col_range][5]
                seven_letter = col[col_range][6]
                eeg_letter = col[col_range][7]

                worksheet.write('%s%s'%(one_letter,row), 'NAMA', left_format)
                worksheet.merge_range('%s%s:%s%s'%(two_letter,row,five_letter,row),'AGUNG PURWANTO',left2_format)
                worksheet.write('%s%s'%(seven_letter,row),'16-Mar19',right_format)

                worksheet.write('%s%s'%(one_letter,row+1), 'NIK', left_format)
                worksheet.merge_range('%s%s:%s%s'%(two_letter,row+1,five_letter,row+1),'20140100130',left2_format)
                worksheet.write('%s%s'%(seven_letter,row+1),'CIMB',left2_format)

                worksheet.write('%s%s'%(one_letter,row+2), 'DIV', left_format)
                worksheet.merge_range('%s%s:%s%s'%(two_letter,row+2,five_letter,row+2),'WP1-FN', left2_format)

                worksheet.write('%s%s'%(one_letter,row+3), 'G POKOK', left_format)
                worksheet.write('%s%s'%(two_letter,row+3), '1', center_format)
                worksheet.write('%s%s'%(two_letter,row+4), '2', center_format)
                worksheet.write('%s%s'%(tree_letter,row+3), '8', center_format)
                worksheet.write('%s%s'%(tree_letter,row+4), '8', center_format)
                worksheet.write('%s%s'%(four_letter,row+3), 'X', center_format)
                worksheet.write('%s%s'%(four_letter,row+4), 'X', center_format)
                worksheet.write('%s%s'%(five_letter,row+3), '58.000', right_format)
                worksheet.write('%s%s'%(five_letter,row+4), '58.000', right_format)
                worksheet.write('%s%s'%(six_letter,row+3), "'=", center_format)
                worksheet.write('%s%s'%(six_letter,row+4), "'=", center_format)
                worksheet.write('%s%s'%(seven_letter,row+3), '500.000', right_format)
                worksheet.write('%s%s'%(seven_letter,row+4), '500.000', right_format)

                worksheet.write('%s%s'%(one_letter,row+5), 'PREMI', left_format)
                worksheet.write('%s%s'%(two_letter,row+5), '1', center_format)
                worksheet.write('%s%s'%(two_letter,row+6), '2', center_format)
                worksheet.write('%s%s'%(tree_letter,row+5), '7', center_format)
                worksheet.write('%s%s'%(tree_letter,row+6), '7', center_format)
                worksheet.write('%s%s'%(four_letter,row+5), 'X', center_format)
                worksheet.write('%s%s'%(four_letter,row+6), 'X', center_format)
                worksheet.write('%s%s'%(five_letter,row+5), '20.000', right_format)
                worksheet.write('%s%s'%(five_letter,row+6), '20.000', right_format)
                worksheet.write('%s%s'%(six_letter,row+5), "'=", center_format)
                worksheet.write('%s%s'%(six_letter,row+6), "'=", center_format)
                worksheet.write('%s%s'%(seven_letter,row+5), '200.000', right_format)
                worksheet.write('%s%s'%(seven_letter,row+6), '200.000', right_format)

                worksheet.write('%s%s'%(one_letter,row+7), 'BNS MG', left_format)
                worksheet.write('%s%s'%(two_letter,row+7), '1', center_format)
                worksheet.write('%s%s'%(two_letter,row+8), '2', center_format)
                worksheet.write('%s%s'%(seven_letter,row+7), '30.000', right_format)
                worksheet.write('%s%s'%(seven_letter,row+8), '30.000', right_format)

                worksheet.write('%s%s'%(one_letter,row+9),'BNS BLN',left_format)
                worksheet.write('%s%s'%(seven_letter,row+9),'0')

                worksheet.write('%s%s'%(one_letter,row+10), 'U LMBR', left_format)
                worksheet.write('%s%s'%(two_letter,row+10), '1', center_format)
                worksheet.write('%s%s'%(two_letter,row+11), '2', center_format)
                worksheet.write('%s%s'%(tree_letter,row+10), '0', center_format)
                worksheet.write('%s%s'%(tree_letter,row+11), '0', center_format)
                worksheet.write('%s%s'%(four_letter,row+10), 'X', center_format)
                worksheet.write('%s%s'%(four_letter,row+11), 'X', center_format)
                worksheet.write('%s%s'%(five_letter,row+10), '16.741', right_format)
                worksheet.write('%s%s'%(five_letter,row+11), '16.741', right_format)
                worksheet.write('%s%s'%(six_letter,row+10), "'=", center_format)
                worksheet.write('%s%s'%(six_letter,row+11), "'=", center_format)
                worksheet.write('%s%s'%(seven_letter,row+10), '0', right_format)
                worksheet.write('%s%s'%(seven_letter,row+11), '0', right_format)

                worksheet.write('%s%s'%(seven_letter,row+12), '1.200.000',right_format)

                worksheet.write('%s%s'%(one_letter,row+13),'U MKN',left_format)
                worksheet.write('%s%s'%(tree_letter,row+13),'14',center_format)
                worksheet.write('%s%s'%(four_letter,row+13),'X',center_format)
                worksheet.write('%s%s'%(five_letter,row+13),'7.500',right_format)
                worksheet.write('%s%s'%(six_letter,row+13),"'=",center_format)
                worksheet.write('%s%s'%(seven_letter,row+13),'100.000',right_format)
                worksheet.write('%s%s'%(eeg_letter,row+13),'-',center_format)
                col_range += 1

                worksheet.write('%s%s'%(seven_letter,row+14),'1.100.000',right_format)

                worksheet.write('%s%s'%(one_letter,row+15),'S POKOK',left_format)
                worksheet.write('%s%s'%(seven_letter,row+15),'50000',right_format)

                worksheet.write('%s%s'%(one_letter,row+16),'S WAJIB',left_format)
                worksheet.write('%s%s'%(seven_letter,row+16),0,right_format)

                worksheet.write('%s%s'%(one_letter,row+17),'KB KAS',left_format)
                worksheet.write('%s%s'%(seven_letter,row+17),0,right_format)

                worksheet.write('%s%s'%(one_letter,row+18),'KORESI',left_format)
                worksheet.write('%s%s'%(seven_letter,row+18),'15000',right_format)

                worksheet.write('%s%s'%(five_letter,row+19),'TOTAL',center1_format)
                worksheet.write('%s%s'%(seven_letter,row+19),'1299000',right_format)

                worksheet.write('%s%s'%(five_letter,row+20),'REAL',center1_format)
                worksheet.merge_range('%s%s:%s%s'%(six_letter,row+20,seven_letter,row+20),'1300000',right1_format)

                worksheet.write('%s%s'%(five_letter,row+21),'CIMB',center1_format)
                worksheet.merge_range('%s%s:%s%s'%(six_letter,row+21,seven_letter,row+21),'1300000',right1_format)

                worksheet.write('%s%s'%(five_letter,row+22),'CASH',center1_format)
                worksheet.merge_range('%s%s:%s%s'%(six_letter,row+22,seven_letter,row+22),'-',right1_format)

                worksheet.write('%s%s'%(five_letter,row+23),'TOTAL',center1_format)
                worksheet.merge_range('%s%s:%s%s'%(six_letter,row+23,seven_letter,row+23),'1300000',right1_format)

                worksheet.merge_range('%s%s:%s%s'%(one_letter,row+24,eeg_letter,row+25),'SATU JUTA TIGA RATUS RIBU',center_format)

                worksheet.write('%s%s'%(one_letter,row+26),'Payroll',left_format)
                worksheet.merge_range('%s%s:%s%s'%(tree_letter,row+26,four_letter,row+26),'Finance',left2_format)
                worksheet.merge_range('%s%s:%s%s'%(six_letter,row+26,seven_letter,row+26),'Checker',left2_format)


            row += 28
            desain_brand_ids -= five_desain_ids

        #sampai sini
        #import pdb;pdb.set_trace()
        workbook.close()
        result = base64.encodestring(fp.getvalue())
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        filename = 'Prekat %s'%(date_string)
        filename += '%2Exlsx'
        self.write({'file_data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=file_data&download=true&filename="+filename
        return {
            'name': 'Prekat',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }


    # @api.multi
    # def download_slip(self):
    #     #ctx = self._context.copy()
    #     #obj = self.env['hr.payslip']
    #     #for po in ctx.get('active_ids',[]):
    #     #    po_id = obj.browse(po)
    #     #    if po_id.order_line and (po_id.state == 'draft'):
    #     #        po_id.signal_workflow('purchase_confirm')

    #     fp = StringIO()
    #     # create an new excel file and add a worksheet
    #     workbook = xlsxwriter.Workbook(fp)
    #     worksheet = workbook.add_worksheet('Report payroll')

    #     #konten di sini

    #     merge_format_left = workbook.add_format({
    #         'bold': 0,
    #         'border': 1,
    #         'align': 'left',
    #         'valign': 'vleft',
    #         'fg_color': 'white'
    #     })
    #     merge_format_right = workbook.add_format({
    #         'bold': 0,
    #         'border': 1,
    #         'align': 'right',
    #         'valign': 'right',
    #         'fg_color': 'white'
    #     })
    #     merge_format = workbook.add_format({
    #         'bold': 1,
    #         'border': 1,
    #         'align': 'center',
    #         'valign': 'vcenter',
    #         'fg_color': 'white'
    #     })
    #     merge_format1 = workbook.add_format({
    #         'bold': 0,
    #         'border': 1,
    #         'align': 'center',
    #         'valign': 'vcenter',
    #         'fg_color': 'white'
    #     })
    #     merge_format_admin = workbook.add_format({
    #         'bold': 1,
    #         'border': 1,
    #         'align': 'right',
    #         'valign': 'vright',
    #         'fg_color': 'DBE5F1'
    #     })
    #     import pdb;pdb.set_trace()
    #     worksheet.set_column('A', 20)
    #     worksheet.set_column('B',5)
    #     worksheet.set_column('c',5)
    #     worksheet.set_column('D',5)
    #     worksheett.set_column('E',20)
    #     worksheet.set_column('F',5)
    #     worksheet.set_column('G',20)
    #     worksheet.set_column('H',5)


    #     worksheet.write('A1','NAMA',merge_format_left)
    #     worksheet.write('A2','NIK',merge_format_left)
    #     worksheet.write('G2','CIMB',merge_format_left)
    #     worksheet.write('A3','DIV',merge_format_left)
    #     worksheet.write('A4','G POKOK',merge_format)
    #     worksheet.write('B4','1')
    #     worksheet.write('B5','2')
    #     worksheet.write('D4','X')
    #     worksheet.write('D5','X')
    #     worksheet.write('F4','=')
    #     worksheet.write('F5','=')
    #     worksheet.write('A6','PREMI',merge_format)
    #     worksheet.write('C6','1')
    #     worksheet.write('F6','=')
    #     worksheet.write('C7','2')
    #     worksheet.write('F7','=')
    #     worksheet.write('D6','X')
    #     worksheet.write('D7','X')
    #     worksheet.write('A8','BNS MG',merge_format)
    #     worksheet.write('B8','1')
    #     worksheet.write('B9','2')
    #     worksheet.write('A10','BNS BLN',merge_format)
    #     worksheet.write('A11','U LMBR',merge_format)
    #     worksheet.write('C11','1')
    #     worksheet.write('C12','2')
    #     worksheet.write('D11','X')
    #     worksheet.write('D12','X')
    #     worksheet.write('F11','=')
    #     worksheet.write('F12','=')
    #     worksheet.write('A14','U MKN',merge_format)
    #     worksheet.write('D7','X')
    #     worksheet.write('F7','=')
    #     worksheet.write('A16','S POKOK',merge_format)
    #     worksheet.write('K1','S WAJIB',merge_format_admin)
    #     worksheet.write('L1','KB KAS',merge_format)
    #     worksheet.write('M1','KOREKSI',merge_format)

    #     # dari database
    #     worksheet.merge_range('B1:E1','AGUNG PURWANTO',merge_format)
    #     worksheet.merge_range('B2:E2','2014010130',merge_format)
    #     worksheet.merge_range('B3:E3','WP1-FN')
    #     worksheet.write('C4','7')
    #     worksheet.write('E4','79.857')
    #     worksheet.write('G4','500.000')
    #     worksheet.write('C5','7')
    #     worksheet.write('E5','79.857')
    #     worksheet.write('G5','500.000')
    #     worksheet.write('C6','7')
    #     worksheet.write('E6','28.857')
    #     worksheet.write('G6','500.000')
    #     worksheet.write('C7','7')
    #     worksheet.write('E7','28.857')
    #     worksheet.write('G7','500.000')
    #     worksheet.write('G10','100.000')
    #     worksheet.write('C11','1')
    #     worksheet.write('G11','100.000')
    #     worksheet.write('C12','0')
    #     worksheet.write('G12','100.000')
    #     worksheet.write('G13','1.000.000')
    #     worksheet.write('C14','11')
    #     worksheet.write('G10','100.000')



    #     workbook.close()
    #     result = base64.encodestring(fp.getvalue())
    #     date_string = self.get_default_date_model().strftime("%Y-%m-%d")
    #     filename = 'Slip Gaji'
    #     filename += '%2Exlsx'
    #     self.write({'file_data':result})
    #     url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=file_data&download=true&filename="+filename
    #     return {
    #         'name': 'Slip Payslip',
    #         'type': 'ir.actions.act_url',
    #         'url': url,
    #         'target': 'new',
    #     }

    @api.model
    def default_get(self, fields_list):
        warning = ''
        skipped = []
        ctx = self.env.context.copy()
        obj = self.env['hr.payslip']
        for po in ctx.get('active_ids',[]):
            po_id = obj.browse(po)
            skipped.append(po_id.name)
        if skipped:
            warning = 'Slip Gaji '+', '.join(skipped)
        return {'warning' : warning}
