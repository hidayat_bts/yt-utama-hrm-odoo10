# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2018 widianajuniar@gmail.com
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
import calendar
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, exceptions, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_is_zero, float_compare
from odoo.exceptions import ValidationError, RedirectWarning, UserError
from collections import OrderedDict



class HRKuota(models.Model):
    _name = 'hr.kuota'
    _order = "date desc" 


    employee_id = fields.Many2one("hr.employee","Employee")
    nik = fields.Char("NIK", related="employee_id.nik", store=True)
    department_id = fields.Many2one("hr.department","Department", related="employee_id.department_id", store=True)  
    job_id = fields.Many2one("hr.job","Job Title", related="employee_id.job_id", store=True)    
    date =fields.Date("Date")
    total_kuota = fields.Integer(string="Total Kuota")
    sisa_kuota = fields.Integer(string="Sisa Kuota")
    is_active = fields.Boolean("Active", default=True)

HRKuota()